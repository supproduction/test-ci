import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataStorageService} from '../shared/data-storage.service';
import {AuthService} from '../auth/auth.service';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
    isAuth = false;
    private userSub: Subscription;

    constructor(
        private storage: DataStorageService,
        private auth: AuthService,
        private store: Store<fromApp.AppState>
    ) {
    }

    ngOnInit() {
        this.userSub = this.store.select('auth').pipe(
            map(authState => {
                return authState.user;
            }),
        )
        .subscribe(user => {
            this.isAuth = !!user;
        });
    }

    ngOnDestroy() {
        this.userSub.unsubscribe();
    }

    onSaveData() {
        this.storage.storeRecipes();
    }

    onFetchData() {
        this.storage.fetchRecipes().subscribe();
    }

    onLogout() {
        this.auth.logout();
    }
}
