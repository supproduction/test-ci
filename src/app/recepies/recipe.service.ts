import {RecipeModel} from './recipie.model';
import {Injectable} from '@angular/core';
import {ShoppingListService} from '../shopping-list/shopping-list.service';
import {Subject} from 'rxjs';
import {Store} from '@ngrx/store';
import * as SlActions from '../shopping-list/store/shopping-list.actions';
import * as fromApp from '../store/app.reducer';

@Injectable()
export class RecipeService {
    recipesChanged = new Subject();

    private recipes: RecipeModel[] = [];

    constructor(
        private slService: ShoppingListService,
        private store: Store<fromApp.AppState>
    ) {
    }

    setRecipes(recepies) {
        this.recipes = recepies;
        this.recipesChanged.next([...this.recipes]);
    }

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index) {
        return this.recipes[index];
    }

    addToSL(ingredients) {
        // this.slService.addIngredients(ingredients);
        this.store.dispatch(new SlActions.AddIngredients(ingredients));
    }

    addRecipe(recipe) {
        this.recipes = [...this.recipes, recipe];
        this.recipesChanged.next([...this.recipes]);
    }

    updateRecipe(index, recipe) {
        this.recipes[index] = recipe;
        this.recipesChanged.next([...this.recipes]);
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
        this.recipesChanged.next([...this.recipes]);
    }
}
