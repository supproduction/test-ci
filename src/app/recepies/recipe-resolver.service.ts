import {Injectable} from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {RecipeModel} from './recipie.model';
import {DataStorageService} from '../shared/data-storage.service';
import {RecipeService} from './recipe.service';

@Injectable({providedIn: 'root'})
export class RecipeResolverService implements Resolve<RecipeModel[]> {
    constructor(private storage: DataStorageService, private rcS: RecipeService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const recipes = this.rcS.getRecipes();

        if (!recipes.length) {
            return this.storage.fetchRecipes();
        }

        return recipes;
    }
}
