import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {RecipeService} from '../recipe.service';
import {RecipeModel} from '../recipie.model';

@Component({
    selector: 'app-recipe-edit',
    templateUrl: './recipe-edit.component.html',
    styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
    id: number;
    editMode = false;
    recipeForm: FormGroup;

    constructor(private route: ActivatedRoute, private rcSer: RecipeService, private router: Router) {
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.id = +params.id;
            this.editMode = params.id !== undefined;
            this.initForm();
        });
    }

    private initForm() {
        let recipeName = '';
        let imagePath = '';
        let description = '';
        const recipeIngredients = new FormArray([]);

        if (this.editMode) {
            const recipe = this.rcSer.getRecipe(this.id);
            recipeName = recipe.name;
            imagePath = recipe.imagePath;
            description = recipe.description;
            recipe.ingredients.forEach(ingredient => {
                if (!ingredient) {
                    return;
                }

                recipeIngredients.push(
                    new FormGroup({
                        name: new FormControl(ingredient.name, Validators.required),
                        amount: new FormControl(ingredient.amount, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
                    })
                );
            });
        }

        this.recipeForm = new FormGroup({
            name: new FormControl(recipeName, Validators.required),
            imagePath: new FormControl(imagePath, Validators.required),
            description: new FormControl(description, Validators.required),
            ingredients: recipeIngredients
        });
    }

    onSubmit() {
        const newRecipe = new RecipeModel(
            this.recipeForm.value.name,
            this.recipeForm.value.imagePath,
            this.recipeForm.value.description,
            this.recipeForm.value.ingredients
        );

        if (this.editMode) {
            this.rcSer.updateRecipe(this.id, this.recipeForm.value);
        } else {
            this.rcSer.addRecipe(this.recipeForm.value);
        }

        this.onCancel();
    }

    addIngredient() {
        (this.recipeForm.get('ingredients') as FormArray).push(
            new FormGroup({
                name: new FormControl(null, Validators.required),
                amount: new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
            })
        );
    }

    onCancel() {
        this.router.navigate(['../'], {relativeTo: this.route});
    }

    onDeleteIngredient(index) {
        (this.recipeForm.get('ingredients') as FormArray).removeAt(index);
    }

    get getFormControls() {
        return (this.recipeForm.get('ingredients') as FormArray).controls;
    }
}
