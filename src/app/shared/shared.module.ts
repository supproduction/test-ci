import {NgModule} from '@angular/core';
import {AlertComponent} from './alert/alert.component';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner';
import {PlaceholerDirective} from './placeholer.directive';
import {DropdownDirective} from './dropdown.directive';
import {CommonModule} from '@angular/common';

@NgModule({
    declarations: [
        AlertComponent,
        LoadingSpinnerComponent,
        PlaceholerDirective,
        DropdownDirective,
    ],
    imports: [CommonModule],
    exports: [
        AlertComponent,
        LoadingSpinnerComponent,
        PlaceholerDirective,
        DropdownDirective,
    ],
    entryComponents: [
        AlertComponent
    ]
})
export class SharedModule {}
