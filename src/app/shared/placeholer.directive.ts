import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
    selector: '[appPlaceholder]'
})
export class PlaceholerDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}
