import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RecipeService} from '../recepies/recipe.service';
import {map, tap} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';

@Injectable({providedIn: 'root'})
export class DataStorageService {
    constructor(private http: HttpClient, private rcSr: RecipeService, private auth: AuthService) {
    }

    storeRecipes() {
        const recipes = this.rcSr.getRecipes();
        const mockedCol = [
            {
                id: 'name',
                title: 'name',
                sortable: true,
                width: '40%'
            },
            {
                id: 'sku',
                title: 'sku',
                sortable: true
            },
            {
                id: 'id3',
                title: 'id3',
                sortable: true
            },
            {
                id: 'sku3',
                title: 'sku3 | link',
                type: 'link',
                typeOptions: {
                    text: '${context.value}',
                    link: 'http://bla.lol/$context.row.id'
                }
            },
            // {
            //     id: 'description',
            //     title: 'description | layout-flat',
            //     type: 'layout-flat',
            //     typeOptions: {
            //         direction: 'row'
            //     },
            //     children: [
            //         {
            //             type: 'link',
            //             typeOptions: {
            //                 text: '${context.row.name}',
            //                 link: 'http://bla.lol/$context.row.id'
            //             }
            //         },
            //         {
            //             type: 'text',
            //             typeOptions: {
            //                 text: '${context.row.sku} - #${context.row.quantity}'
            //             }
            //         }
            //     ]
            // }
        ];


        const mockedData = {
            data: [
                {
                    name: 'tesst',
                    sku: 'tesst2',
                    id3: 'tesst3',
                    sku3: '124124'
                },
                {
                    name: 'tesst1',
                    sku: 'tesst22',
                    id3: 'tesst34',
                    sku3: 'idasaasf',
                },
                {
                    name: 'tess2t',
                    sku: 'asfasffas',
                    id3: 'tessst3',
                    sku3: '111idasaasf',
                },
            ],
            total: 500,
            pageSize: 50,
            offset: 3,
        };
        this.http.put('https://angular-recipe-24caa.firebaseio.com/data.json', mockedData).subscribe();
        this.http.put('https://angular-recipe-24caa.firebaseio.com/col.json', mockedCol).subscribe();
    }

    fetchRecipes() {
        return this.http
            .get<[]>('https://angular-recipe-24caa.firebaseio.com/recipes.json')
            .pipe(
                map(recipes => {
                    if (!recipes) {
                        return [];
                    }

                    return recipes.map((recipe: any) => {
                        return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
                    });
                }),
                tap(recipes => {
                    this.rcSr.setRecipes(recipes);
                })
            );
    }
}
