import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {AppRoutingModule} from './app-routing.module';
import {RecipeService} from './recepies/recipe.service';
import {ShoppingListService} from './shopping-list/shopping-list.service';
import {AuthComponent} from './auth/auth.component';
import {AuthInterceptorsService} from './auth/auth-interceptors.service';
import {RecipesModule} from './recepies/recipes.module';
import {ShoppingListModule} from './shopping-list/shopping-list.module';
import {SharedModule} from './shared/shared.module';
import {appReducer} from './store/app.reducer';
import {AuthEffects} from './auth/store/auth.effects';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        AuthComponent,
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'serverApp' }),
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        RecipesModule,
        ShoppingListModule,
        SharedModule,
        StoreModule.forRoot(appReducer),
        EffectsModule.forRoot([AuthEffects])
    ],
    providers: [RecipeService, ShoppingListService, {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorsService, multi: true}],
    bootstrap: [AppComponent],
})
export class AppModule {
}
