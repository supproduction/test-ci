import {Action} from '@ngrx/store';
import {IngredientModel} from '../../shared/ingredient.model';

export const ADD_INGREDIENT = '[Shopping List] Add Ingredient';
export const ADD_INGREDIENTS = '[Shopping List] Add Ingredients';
export const UPDATE_INGREDIENTS = '[Shopping List] Update Ingredient';
export const DELETE_INGREDIENT = '[Shopping List] Delete Ingredient';
export const START_EDIT = 'START_EDIT';
export const STOP_EDIT = 'STOP_EDIT';

export class AddIngredient implements Action {
    readonly type = ADD_INGREDIENT;

    constructor(public payload: IngredientModel) {
    }
}

export class AddIngredients implements Action {
    readonly type = ADD_INGREDIENTS;

    constructor(public payload: IngredientModel[]) {
    }
}

export class UpdateIngredients implements Action {
    readonly type = UPDATE_INGREDIENTS;

    constructor(public payload: { index, newIng }) {
    }
}

export class DeleteIngredients implements Action {
    readonly type = DELETE_INGREDIENT;

    constructor(public payload: number) {
    }
}

export class StartEdit implements Action {
    readonly type = START_EDIT;

    constructor(public payload: number) {
    }
}

export class StopEdit implements Action {
    readonly type = STOP_EDIT;
}

export type SlDefActions = StopEdit | StartEdit | AddIngredient | AddIngredients | DeleteIngredients | UpdateIngredients;
