import {IngredientModel} from '../../shared/ingredient.model';
import * as SlActions from './shopping-list.actions';

export interface State {
    ingredients: IngredientModel[];
    editedItem: IngredientModel;
    editedItemIndex: number;
}

const initialState = {
    ingredients: [
        new IngredientModel('apples', 5),
        new IngredientModel('tomatos', 10),
    ],
    editedItem: null,
    editedItemIndex: -1,
};

export function shoppingListReducer(state: State = initialState, action: SlActions.SlDefActions) {
    switch (action.type) {
        case SlActions.ADD_INGREDIENT:
            return {
                ...state,
                ingredients: [...state.ingredients, action.payload]
            };
        case SlActions.ADD_INGREDIENTS:
            return {
                ...state,
                ingredients: [...state.ingredients, ...action.payload]
            };
        case SlActions.UPDATE_INGREDIENTS:
            const {ingredients} = state;
            const {index, newIng} = action.payload;
            ingredients[index] = newIng;

            return {
                ...state,
                ingredients
            };
        case SlActions.DELETE_INGREDIENT:
            return {
                ...state,
                ingredients: state.ingredients.filter((ig, ind) => ind !== action.payload)
            };
        case SlActions.START_EDIT:
            return {
                ...state,
                editedItemIndex: action.payload,
                editedItem: {...state.ingredients[action.payload] }
            };
        case SlActions.STOP_EDIT:
            return {
                ...state,
                editedItemIndex: -1,
                editedItem: null
            };
        default:
            return state;
    }
}
