import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IngredientModel} from '../../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list.service';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import * as SlActions from '../store/shopping-list.actions';
import * as fromApp from '../../store/app.reducer';

@Component({
    selector: 'app-shopping-edit',
    templateUrl: './shopping-edit.component.html',
    styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
    @ViewChild('f', {static: true}) slForm: NgForm;
    subscription: Subscription;
    editMode = false;
    editedItemIndex: number;
    editedItem;

    constructor(
        private slService: ShoppingListService,
        private store: Store<fromApp.AppState>
    ) {
    }

    ngOnInit() {
        this.subscription = this.store.select('sl').subscribe(stateData => {
            if (stateData.editedItemIndex > -1) {
                this.editMode = true;
                this.editedItem = stateData.editedItem;
                this.editedItemIndex = stateData.editedItemIndex;

                this.slForm.setValue({
                    ...this.editedItem
                });

                return;
            }

            this.editMode = false;
        });
        // this.subscription = this.slService.startedEditing.subscribe((index) => {
        //     this.editMode = true;
        //     this.editedItemIndex = index;
        //     this.editedItem = this.slService.getIngredient(index);
        //     this.slForm.setValue({
        //         ...this.editedItem
        //     });
        // });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.store.dispatch(new SlActions.StopEdit());
    }

    onAddItem(f: NgForm) {
        const value = f.value;
        const newIngredient = new IngredientModel(value.name, value.amount);

        if (this.editMode) {
            this.store.dispatch(new SlActions.UpdateIngredients({index: this.editedItemIndex, newIng: newIngredient}));
            // this.slService.updateIngredient(this.editedItemIndex, newIngredient);
        } else {
            // this.slService.addIngredient(newIngredient);
            this.store.dispatch(new SlActions.AddIngredient(newIngredient));
        }

        this.onClear();
    }

    onClear() {
        this.slForm.reset();
        this.editMode = false;
        this.store.dispatch(new SlActions.StopEdit());
    }

    onDelete() {
        this.store.dispatch(new SlActions.DeleteIngredients(this.editedItemIndex));
        // this.slService.deleteIngredient(this.editedItemIndex);
        this.onClear();
    }

}
