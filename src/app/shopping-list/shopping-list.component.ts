import {Component, OnInit, OnDestroy} from '@angular/core';
import {IngredientModel} from '../shared/ingredient.model';
import {ShoppingListService} from './shopping-list.service';
import {Observable, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as SlActions from './store/shopping-list.actions';

@Component({
    selector: 'app-shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls: ['./shopping-list.component.css'],
})
export class ShoppingListComponent implements OnInit, OnDestroy {
    ingredients: Observable<{ ingredients: IngredientModel[] }>;
    private igChanged: Subscription;

    constructor(
        private ingertientsList: ShoppingListService,
        private store: Store<fromApp.AppState>
    ) {
    }

    ngOnInit() {
        this.ingredients = this.store.select('sl');
        // this.ingredients = this.ingertientsList.getIngredients();
        // this.igChanged = this.ingertientsList.ingredientsChanged.subscribe((ingertientsList) => {
        //     this.ingredients = ingertientsList;
        // });
    }

    ngOnDestroy() {
        // this.igChanged.unsubscribe();
    }

    onEditItem(index: number) {
        // this.ingertientsList.startedEditing.next(index);
        this.store.dispatch(new SlActions.StartEdit(index));
    }
}
