import {IngredientModel} from '../shared/ingredient.model';
import {Subject} from 'rxjs';

export class ShoppingListService {
    ingredientsChanged = new Subject<IngredientModel[]>();
    startedEditing = new Subject<number>();
    private ingredients: IngredientModel[] = [];

    getIngredients() {
        return [...this.ingredients];
    }

    getIngredient(index) {
        return this.ingredients[index];
    }

    addIngredient(ingredient: IngredientModel) {
        this.ingredients.push(ingredient);
        this.ingredientsChanged.next([...this.ingredients]);
    }

    updateIngredient(index, newIng) {
        this.ingredients[index] = newIng;
        this.ingredientsChanged.next([...this.ingredients]);
    }

    deleteIngredient(index) {
        this.ingredients.splice(index, 1);
        this.ingredientsChanged.next([...this.ingredients]);
    }

    addIngredients(ingredients) {
        this.ingredients.push(...ingredients);
        this.ingredientsChanged.next([...this.ingredients]);
    }
}
