import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpParams} from '@angular/common/http';
import {AuthService} from './auth.service';
import {exhaustMap, map, take} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';

@Injectable()
export class AuthInterceptorsService implements HttpInterceptor {
    constructor(
        private auth: AuthService,
        private store: Store<fromApp.AppState>
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return this.store.select('auth').pipe(
            take(1),
            map(authState => {
                return authState.user;
            }),
            exhaustMap(user => {
                if (!user) {
                    return next.handle(req);
                }

                const modifReq = req.clone({params: new HttpParams().set('auth', user.token)});

                return next.handle(modifReq);
            })
        );
    }
}
