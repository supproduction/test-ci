import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthService} from './auth.service';
import {Observable, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {AlertComponent} from '../shared/alert/alert.component';
import {PlaceholerDirective} from '../shared/placeholer.directive';
import {Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from './store/auth.actions';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {
    isLoginMode = true;
    isLoading = false;
    error: string = null;
    @ViewChild(PlaceholerDirective, {static: false}) alertHost: PlaceholerDirective;
    private closeSub: Subscription;

    constructor(
        private authSc: AuthService,
        private router: Router,
        private cmpFactory: ComponentFactoryResolver,
        private store: Store<fromApp.AppState>
    ) {
    }

    ngOnInit() {
        this.store.select('auth').subscribe((authState: any) => {
            this.isLoading = authState.loading;
            this.error = authState.authError;

            if (!this.error) {
                return;
            }

            this.showErrorAlert(this.error);
        });
    }

    onSwitchMode() {
        this.isLoginMode = !this.isLoginMode;
    }

    onSubmit(form) {
        if (!form.value) {
            return;
        }

        const {email, password} = form.value;
        this.isLoading = true;
        form.reset();

        let authObs: Observable<any>;

        if (this.isLoginMode) {
            this.store.dispatch(new AuthActions.LoginStart({email, password}));
            // authObs = this.authSc.login(email, password);
        }

        if (!this.isLoginMode) {
            this.store.dispatch(new AuthActions.SighnUpStart({email, password}));
        }


        // authObs.subscribe(
        //     () => {
        //         this.isLoading = false;
        //         this.router.navigate(['/recipes']);
        //     },
        //     errorMessage => {
        //         this.showErrorAlert(errorMessage);
        //         this.error = errorMessage;
        //         this.isLoading = false;
        //     }
        // );
    }

    private showErrorAlert(message) {
        const alertCmpFactory = this.cmpFactory.resolveComponentFactory(AlertComponent);

        const hostViewContainerRef = this.alertHost.viewContainerRef;
        hostViewContainerRef.clear();

        const alertRef = hostViewContainerRef.createComponent(alertCmpFactory);

        alertRef.instance.message = message;
        this.closeSub = alertRef.instance.close.subscribe(() => {
            this.closeSub.unsubscribe();
            hostViewContainerRef.clear();
        });
    }

    ngOnDestroy() {
        if (this.closeSub) {
            this.closeSub.unsubscribe();
        }
    }

    onHandleError() {
        this.error = null;
    }
}
