import {UserModel} from '../user.model';
import * as AuthAction from './auth.actions';


export interface State {
    user: UserModel;
    authError: string;
}

const initialState: State = {
    user: null,
    authError: null
};

export function authReducer(state: State = initialState, action: AuthAction.AuthAction) {
    switch (action.type) {
        case AuthAction.LOGIN:
            const {email, localId, idToken, expDate} = action.payload;
            const user = new UserModel(email, localId, idToken, expDate);

            return {
                ...state,
                user,
                authError: null
            };
        case AuthAction.LOGOUT:
            return {
                ...state,
                user: null
            };
        case AuthAction.LOGIN_START:
            return {
                ...state,
                authError: null
            };
        case AuthAction.LOGIN_FAIL:
            return {
                ...state,
                user: null,
                authError: action.payload
            };
        default:
            return state;
    }
}
