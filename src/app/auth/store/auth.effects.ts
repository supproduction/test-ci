import {Actions, Effect, ofType} from '@ngrx/effects';
import * as AuthAction from './auth.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {of, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

const apiKey = 'AIzaSyDoiLvfPFz_S66JAsvOIfHpz5PF-k_rfR4';

const handleAuthentication = (resData) => {
    const expTime = +resData.expiresIn * 1000;
    const expDate = new Date(new Date().getTime() + expTime);

    return new AuthAction.Login({
        email: resData.email,
        localId: resData.localId,
        idToken: resData.idToken,
        expDate
    });
};

const handleError = (errorRes) => {
    let errorMessage = 'Error';

    if (!errorRes.error || !errorRes.error.error) {
        return of(new AuthAction.LoginFail(errorMessage));
    }

    switch (errorRes.error.error.message) {
        case 'EMAIL_EXISTS':
            errorMessage = 'EMAIL EXISTS';
            break;
    }

    return of(new AuthAction.LoginFail(errorMessage));
};

@Injectable()
export class AuthEffects {
    @Effect()
    authSignUp = this.actions$.pipe(
        ofType(AuthAction.SIGHNUP_START),
        switchMap((authData: any) => {
                return this.http.post(
                    `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${apiKey}`,
                    {
                        email: authData.payload.email,
                        password: authData.payload.password,
                        returnSecureToken: true
                    }
                ).pipe(
                    map(handleAuthentication),
                    catchError(handleError)
                );
            }
        )
    );

    @Effect()
    authLogin = this.actions$.pipe(
        ofType(AuthAction.LOGIN_START),
        switchMap((authData: any) => {
            return this.http.post(
                `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${apiKey}`,
                {
                    email: authData.payload.email,
                    password: authData.payload.password,
                    returnSecureToken: true
                }
            ).pipe(
                map(handleAuthentication),
                catchError(handleError)
            );
        }),
    );

    @Effect({dispatch: false})
    authSuccess = this.actions$.pipe(
        ofType(AuthAction.LOGIN),
        tap(() => {
            this.router.navigate(['/']);
        })
    );

    constructor(private actions$: Actions, private http: HttpClient, private router: Router) {
    }
}
