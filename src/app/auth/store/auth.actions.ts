import {Action} from '@ngrx/store';

export const LOGIN = 'LOGIN';
export const LOGIN_START = 'LOGIN_START';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT = 'LOGOUT';
export const SIGHNUP_START = 'SIGHNUP_START';

export class Login implements Action {
    readonly type = LOGIN;

    constructor(public payload: { email, localId, idToken, expDate }) {
    }
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export class LoginStart implements Action {
    readonly type = LOGIN_START;

    constructor(public payload: { email, password }) {
    }
}

export class LoginFail implements Action {
    readonly type = LOGIN_FAIL;

    constructor(public payload: string) {
    }
}

export class SighnUpStart implements Action {
    readonly type = SIGHNUP_START;

    constructor(public payload: { email, password }) {
    }
}


export type AuthAction = Login | Logout | LoginStart | LoginFail | SighnUpStart;
