import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {BehaviorSubject, throwError} from 'rxjs';
import {UserModel} from './user.model';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from './store/auth.actions';

@Injectable({providedIn: 'root'})
export class AuthService {
    apiKey = 'AIzaSyDoiLvfPFz_S66JAsvOIfHpz5PF-k_rfR4';
    // user = new BehaviorSubject<UserModel>(null);
    private tokenExpTimer: any;

    constructor(
        private http: HttpClient,
        private router: Router,
        private store: Store<fromApp.AppState>
    ) {
    }

    signup(email, password) {
        return this.http.post(
            `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${this.apiKey}`,
            {
                email,
                password,
                returnSecureToken: true
            }
        ).pipe(
            catchError(this.handleError),
            tap(resData => this.handleAuthentication(email, resData))
        );
    }

    login(email, password) {
        return this.http.post(
            `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${this.apiKey}`,
            {
                email,
                password,
                returnSecureToken: true
            }
        ).pipe(
            catchError(this.handleError),
            tap(resData => this.handleAuthentication(email, resData))
        );

    }

    logout() {
        this.store.dispatch(new AuthActions.Logout());
        // this.user.next(null);
        this.router.navigate(['/auth']);
        localStorage.removeItem('userData');

        if (!this.tokenExpTimer) {
            return;
        }

        clearTimeout(this.tokenExpTimer);
        this.tokenExpTimer = null;
    }

    autoLogout(expDuration: number) {
        this.tokenExpTimer = setTimeout(() => {
            this.logout();
        }, expDuration);
    }

    autoLogin() {
        const userData = JSON.parse(localStorage.getItem('userData'));

        if (!userData) {
            return;
        }

        const expDate = new Date(userData._tokenExpDate);
        const loadedUser = new UserModel(userData.email, userData.id, userData._token, expDate);

        if (loadedUser.token) {
            const expTime = expDate.getTime() - new Date().getTime();

            this.store.dispatch(new AuthActions.Login({
                email: userData.email,
                localId: userData.id,
                idToken: userData._token,
                expDate
            }));
            // this.user.next(loadedUser);
            this.autoLogout(expTime);
        }
    }

    private handleAuthentication(email, resData: any) {
        const expTime = +resData.expiresIn * 1000;
        const expDate = new Date(new Date().getTime() + expTime);
        const user = new UserModel(email, resData.localId, resData.idToken, expDate);

        this.store.dispatch(new AuthActions.Login({
            email,
            localId: resData.localId,
            idToken: resData.idToken,
            expDate
        }));
        // this.user.next(user);
        this.autoLogout(expTime);
        localStorage.setItem('userData', JSON.stringify(user));
    }

    private handleError(errorRes: HttpErrorResponse) {
        let errorMessage = 'Error';

        if (!errorRes.error || !errorRes.error.error) {
            return throwError(errorMessage);
        }

        switch (errorRes.error.error.message) {
            case 'EMAIL_EXISTS':
                errorMessage = 'EMAIL EXISTS';
                break;
        }

        return throwError(errorRes.error.error.message);
    }
}
